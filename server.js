const libgen = require("libgen");
const express = require("express");
const app = express();
const {
    text
} = require("express");
const http = require('http')
const cheerio = require('cheerio');


const PORT = 3003;
console.log("port: " + PORT);
console.log("-----");
const site_url = "93.174.95.27";


app.get("/", async (req, res, next) => {

    try {

        const options = {
            mirror: "http://"+site_url,
            query: req.query.query,
            count: req.query.count,
            offset: req.query.offset,
            search_in: req.query.search_in,
            sort_by: req.query.sort_by,
            reverse: req.query.reverse

        }
        res.json({'mirror': "http://"+site_url, 'content': await libgen.search(options)})
       

    } catch (err) {
        next(err)
    }

});


app.get("/download", async (req, resp, next) => {

   
    const options = {
        hostname: "libgen.gs",
        path: '/ads.php?md5=' + req.query.md5,
        method: 'GET'
    }


    const requests = http.request(options, res => {
        res.on('data', body => {
            var data_store = {};
            data_store["download"] = [];

            const $ = cheerio.load(body);

            var covers = $('tbody>tr>td>a').attr("href")

            if (covers != undefined){
                data_store["download"] = covers
                return resp.send(data_store);
            }



        });});


        
       

        requests.on('error', error => {
            console.error(error)
        })

        requests.end()


});

const server = app.listen(process.env.PORT || PORT, function () {});
